# Offline Charging
bin/kpoc_charger
etc/init/kpoc_charger.rc
lib/libshowlogo.so
lib/libsysenv_system.so

# MTK-IMS | from full_certus-userdebug 10 QP1A.190711.020 mp1V91561 test-keys
>>>>>>> cc772d7... certus: Build ims boot jars
-framework/mediatek-common.jar
-framework/mediatek-framework.jar
-framework/mediatek-ims-base.jar
-framework/mediatek-ims-common.jar
-framework/mediatek-telecom-common.jar
-framework/mediatek-telephony-base.jar
-framework/mediatek-telephony-common.jar
-priv-app/ImsService/ImsService.apk
bin/vtservice
etc/init/init.vtservice.rc
framework/mediatek-ims-extension-plugin.jar
framework/mediatek-ims-legacy.jar
lib/libcomutils.so
lib/libimsma.so
lib/libimsma_adapt.so
lib/libimsma_rtp.so
lib/libimsma_socketwrapper.so
lib/libmtk_vt_service.so
lib/libmtk_vt_wrapper.so
lib/libmtkaudio_utils.so
lib/libmtkavenhancements.so
lib/libmtklimiter.so
lib/libmtkperf_client.so
lib/libmtkshifter.so
lib/libsignal.so
lib/libsink.so
lib/libsource.so
lib/libvcodec_cap.so
lib/libvcodec_capenc.so
lib/libvt_avsync.so
lib/vendor.mediatek.hardware.videotelephony@1.0.so
lib64/libmtk_vt_wrapper.so
lib64/libmtkaudio_utils.so
lib64/libmtkavenhancements.so
lib64/libmtklimiter.so
lib64/libmtkperf_client.so
lib64/libmtkshifter.so
lib64/libvcodec_cap.so
lib64/libvcodec_capenc.so

# Mediatek EngineerMode | from full_certus-userdebug 10 QP1A.190711.020 mp1V91561 test-keys
priv-app/EngineerMode/EngineerMode.apk
lib/libem_support_jni.so
lib/libem_usb_jni.so
lib/libem_wifi_jni.so
lib64/libem_support_jni.so
lib64/libem_usb_jni.so
lib64/libem_wifi_jni.so
